(in-package #:parenscript6)

(defun code (file js &optional print-output)
  (with-open-file (output file
                          :direction :output
                          :if-exists :supersede)
    (format output js)
    (if print-output
        (format t js))))




;; Exercise 1
(code
 #p"javascript.js"
 (ps
   (chain *react-d-o-m (render
                        (jsx "ul" ()
                            ((jsx "li" () ("This"))
                             (jsx "li" () ("seems"))
                             (jsx "li" () ("to work."))))
                        (chain document (get-element-by-id "app"))))))




;; Functional Components
(code
 #p"javascript.js"
 (ps
   (defun *my-app () (jsx "ul" ()
                         ((jsx "li" () ("This"))
                          (jsx "li" () ("also"))
                          (jsx "li" () ("works.")))))

   (chain *react-d-o-m (render
                        (jsx *my-app)
                        (chain document (get-element-by-id "app"))))))




;; Exercise 2
(code
 #p"javascript.js"
 (ps
   (defun *my-info ()
     (jsx "div" ()
         ((jsx "h1" () ("Markus"))
          (jsx "p" () ("Some text."))
          (jsx "ul" ()
              ((jsx "li" () ("J"))
               (jsx "li" () ("S"))
               (jsx "li" () ("X")))))))

   (chain *react-d-o-m (render
                        (jsx *my-info)
                        (chain document (get-element-by-id "app"))))))




;; Move Functions in seperate Files




;; Parent/Child Components
(code
 #p"javascript.js"
 (ps
   (defun *footer ()
     (jsx "footer" ()
         ((jsx "h3" () ("This is my Footer!")))))
   
   (defun *app ()
     (jsx "div" ()
         ((jsx "nav" ()
              ((jsx "h1" () ("Hello a third time!"))
               (jsx "ul" ()
                   ((jsx "li" () ("Thing 1"))
                    (jsx "li" () ("Thing 2"))
                    (jsx "li" () ("Thing 3"))))))
          (jsx "main" ()
              ((jsx "p" () ("This is where most of my content will go..."))))
          (jsx *footer))))
   
   (chain *react-d-o-m (render
                        (jsx *app)
                        (chain document (get-element-by-id "app"))))))




;; Exercise 3
(code
 #p"javascript.js"
 (ps
   (defun *navbar ()
     (jsx "nav" ()
         ((jsx "h1" () ("This is the Navigation")))))

   (defun *main-content ()
     (jsx "main" ()
         ((jsx "h1" () ("This is the Main Content."))
          (jsx "p" () ("This is content."))
          (jsx "p" () ("This is more content.")))))

   (defun *footer ()
     (jsx "footer" ()
         ((jsx "h1" () ("This is the Footer.")))))
   
   (defun *app ()
     (jsx "div" ()
         ((jsx *navbar)
          (jsx *main-content)
          (jsx *footer))))

   (chain *react-d-o-m (render
                        (jsx *app)
                        (chain document (get-element-by-id "app"))))))




;; Styling with CSS
(code
 #p"javascript.js"
 (ps
   (defun *header ()
     (jsx "header" (class-name "navbar")
         ("This is the header.")))
   
   (defun *main-content ()
     (jsx "main" () ("This is the main content.")))
   
   (defun *footer ()
     (jsx "footer" () ("This is the footer.")))
   
   (defun *app ()
     (jsx "div" ()
         ((jsx *header)
          (jsx *main-content)
          (jsx *footer))))
   
   (chain *react-d-o-m (render
                        (jsx *app)
                        (chain document (get-element-by-id "app"))))))

(code
 #p"style.css"
 (stringify
  "body { margin: 0; } "
  ".navbar { 
height: 100px;
background-color: #333;
color: whitesmoke;
margin-bottom: 15px;
text-align: center;
font-size: 30px;
display: flex;
justify-content: center;
align-items: center; }"))




;; JSX and JavaScript
(code
 #p"javascript.js"
 (ps
   (defun *app ()
     (var first-name "Bob")
     (var last-name "Ziroll")
     (var date (new (*date)))
     (jsx "h1" () ("Hello "
                   (+ first-name " " last-name "! ")
                   "It is currently about "
                   (rem (chain date (get-hours)) 12)
                   " o'clock")))

   (chain *react-d-o-m (render
                        (jsx *app)
                        (chain document (get-element-by-id "app"))))))




;; Inline Styles
(code
 #p"javascript.js"
 (ps
   (defun *header ()
     (var styles (create color "#ffac00"
                         background-color "#ff2d00"
                         font-size 24
                         font-size "24px"))
     (jsx "header" (style styles)
         ("This is the header.")))

   ;; dynamic Style
   (defun *main-content ()
     (var date (new (*date)))
     (var hours (chain date (get-hours)))
     (var time-of-day)
     (var styles2 (create font-size 30))
     (cond ((< hours 12) (setf time-of-day "morning") (setf (@ styles2 color) "#04756F"))
           ((>= 17 hours 12) (setf time-of-day "afternoon") (setf (@ styles2 color) "#8914A3"))
           (t (setf time-of-day "night") (setf (@ styles2 color) "#D90000")))
     (jsx "main" (style styles2) ((+ "Good " time-of-day "!"))))
   
   (defun *footer ()
     (jsx "footer" () ("This is the footer.")))
   
   (defun *app ()
     (jsx "div" ()
         ((jsx *header)
          (jsx *main-content)
          (jsx *footer))))
   
   (chain *react-d-o-m (render
                        (jsx *app)
                        (chain document (get-element-by-id "app"))))))




;; Props
(code
 #p"javascript.js"
 (ps
   (defun *contact-card (props)
     (jsx "div" (class-name "contact-card")
         ((jsx "img" (src (@ props contact img-url)))
          (jsx "h3" () ((@ props contact name)))
          (jsx "p" () ("Phone: " (@ props contact phone)))
          (jsx "p" () ("Email: " (@ props contact email))))))
   
   (defun *app ()
     (jsx "div" (class-name "contacts")
         ((jsx *contact-card (contact (create
                                       name "Mr. Whiskerson"
                                       img-url "http://placekitten.com/300/200"
                                       phone "(212) 555-1234"
                                       email "mr.whiskaz@catnap.meow")))
          (jsx *contact-card (contact (create
                                       name "Fluffykins"
                                       img-url "http://placekitten.com/400/200"
                                       phone "(212) 555-1234"
                                       email "mr.whiskaz@catnap.meow")))
          (jsx *contact-card (contact (create
                                       name "Destroyer"
                                       img-url "http://placekitten.com/300/300"
                                       phone "(212) 555-1234"
                                       email "mr.whiskaz@catnap.meow")))
          (jsx *contact-card (contact (create
                                       name "Felix"
                                       img-url "http://placekitten.com/200/200"
                                       phone "(212) 555-1234"
                                       email "mr.whiskaz@catnap.meow"))))))

   (chain *react-d-o-m (render
                        (jsx *app)
                        (chain document (get-element-by-id "app"))))))




;; Props and Styling Practice
(code
 #p"javascript.js"
 (ps
   (defun *joke (props)
     (jsx "div" ()
         ((if (@ props question)
              (jsx "h1" () ("Question: " (@ props question))))
          (jsx "p" () ((@ props punchline))))))
   
   (defun *app ()
     (jsx "div" ()
         ((jsx *joke (question "a" punchline "b"))
          (jsx *joke (punchline "c"))
          (jsx *joke (question "d" punchline "e"))
          (jsx *joke (punchline "f"))
          (jsx *joke (question "g" punchline "h")))))
   
   (chain *react-d-o-m (render
                        (jsx *app)
                        (chain document (get-element-by-id "app"))))))




;; Mapping Components
(code
 #p"javascript.js"
 (ps
   (var jokes-data (list (create
                          id 1
                          question "a"
                          punchline "b")
                         (create
                          id 2
                          question "c"
                          punchline "d")
                         (create
                          id 3
                          question "e"
                          punchline "f")
                         (create
                          id 4
                          question "g"
                          punchline "h")))
   
   (defun *joke (props)
     (jsx "div" ()
         ((if (@ props question)
              (jsx "h1" () ("Question: " (@ props question))))
          (jsx "p" () ((@ props punchline))))))
   
   (defun *app ()
     (var joke-components (chain jokes-data (map #'(lambda (joke)
                                                     (jsx *joke (id (@ joke id)
                                                                    question (@ joke question)
                                                                    punchline (@ joke punchline)))))))
     (jsx "div" () (joke-components)))
   
   (chain *react-d-o-m (render
                        (jsx *app)
                        (chain document (get-element-by-id "app"))))))




;; Class-based Components
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component))
    (new-method render ()
      (jsx "div" ()
          ((jsx "h1" () ("Code goes here"))))))

  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Exercise 4
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component))
    (new-method render ()
      (jsx "div" ()
          ((jsx *header (username "Bob"))
           (jsx *greeting)))))

  (new-class *header (:extends (@ *react *component))
    (new-method render ()
      (jsx "header" ()
          ((jsx "p" () ("Welcome, "
                        (r-props username)))))))

  (new-class *greeting (:extends (@ *react *component))
    (new-method render ()
      (var date (new (*date)))
      (var hours (chain date (get-hours)))
      (var time-of-day)
      (cond ((< hours 12) (setf time-of-day "morning"))
            ((>= 17 hours 12) (setf time-of-day "afternoon"))
            (t (setf time-of-day "night")))
      (jsx "h1" () ((+ "Good " time-of-day " to you, sir or madam!")))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; State
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (setf (r-state) (create
                                                      answer "Yes")))))
    (new-method render ()
      (jsx "div" ()
          ((jsx "h1" () ("Is state important to know? "
                         (r-state answer)))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Exercise 5
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (setf (r-state) (create
                                                      name "Bob"
                                                      age 33)))))
    (new-method render ()
      (jsx "div" ()
          ((jsx "h1" () ((r-state name)))
           (jsx "h3" () ((r-state age) " years old"))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Exercise 6
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (setf (r-state) (create
                                                      is-logged-in f)))))
    (new-method render ()
      (jsx "div" ()
          ((jsx "h1" () ("You are currently logged "
                         (if (r-state is-logged-in)
                             "in"
                             "out") "."))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Events in React
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component))
    (new-method render ()
      (jsx "div" ()
          ((jsx "img" (src "https://www.fillmurray.com/200/100"
                           on-mouse-over (lambda ()
                                           ((@ console log) "You entered the picture."))))
           (jsx "br")
           (jsx "br")
           (jsx "button" (on-click (lambda ()
                                     ((@ console log) "I have been clicked.")))
               ("Click me."))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Changing State
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (ps-bind handle-click)
                                     (setf (r-state) (create
                                                      count 0)))))
    (new-method handle-click ()
      ((@ this set-state) (lambda (prev-state)
                            (create
                             count (1+ (@ prev-state count))))))
    
    (new-method render ()
      (jsx "div" ()
          ((jsx "h1" () ((r-state count)))
           (jsx "button" (on-click (@ this handle-click)) ("Change!"))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Lifecycle Methods Part 1
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((setup)
                                     (setf (r-state) (create)))))
    (new-method component-did-mount () ; after mounted, before displayed
      ())

    (new-method should-component-update (next-props next-state) ; check if React needs to update
      ( ; Return true if you want to update
       ))

    (new-method component-will-unmount () ; before a component disappears
      ( ; remove event listeners
        ; teardown or cleanup your code
       ))
    
    (new-method render ()
      (jsx "div" () ("Code goes here."))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Lifecycle Methods Part 2
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (setf (r-state) (create)))))
    (static (new-method get-derived-state-from-props (props state)
              ( ;; shouldn't be needed
                ;; https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html
               )))

    (new-method get-snapshot-before-update () ; create a backup of the current data
      ())
    
    (new-method render ()
      (jsx "div" () ("Code goes here."))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Conditional Rendering
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *conditional (:extends (@ *react *component))
    (new-method render ()
      (jsx "div" ()
          ((jsx "h1" () ("Navbar"))
           (if (r-props is-loading)
               (jsx "h1" () ("Loading..."))
               (jsx "h1" () ("Finished")))
           (jsx "h1" () ("Footer"))))))
  
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (ps-bind loading-finished)
                                     (setf (r-state) (create
                                                      is-loading true)))))
    (new-method loading-finished ()
      ((@ this set-state) (create
                           is-loading false)))

    (new-method component-did-mount ()
      (set-timeout (@ this loading-finished) 1500))

    (new-method render ()
      (jsx "div" ()
          ((jsx *conditional (is-loading (r-state is-loading)))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))

;; better: Component that has state controls what to display
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *conditional (:extends (@ *react *component))
    (new-method render ()
      (jsx "h1" () ("Finished"))))
  
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (ps-bind loading-finished)
                                     (setf (r-state) (create
                                                      is-loading true)))))
    (new-method loading-finished ()
      ((@ this set-state) (create
                           is-loading false)))

    (new-method component-did-mount ()
      (set-timeout (@ this loading-finished) 1500))

    (new-method render ()
      (jsx "div" ()
          ((jsx "h1" () ("Navbar"))
           (if (r-state is-loading)
               (jsx "h1" () ("Loading"))
               (jsx *conditional))
           (jsx "h1" () ("Footer"))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Conditional Rendering Part 2
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (setf (r-state) (create
                                                      unread-messages '("Call your mom!"
                                                                        "New spam email available."))))))
    (new-method render ()
      (jsx "div" ()
          ((jsx "h2" () ((and (> (r-state unread-messages length) 0)
                              (+ "You have " (r-state unread-messages length) " unread messages."))))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Exercise 7
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (ps-bind change-login)
                                     (setf (r-state) (create
                                                      logged-in f)))))
    (new-method change-login ()
      ((@ this set-state) (lambda (prev-state)
                            (create
                             logged-in (not (@ prev-state logged-in))))))

    (new-method render ()
      (jsx "div" ()
          ((jsx "p" () ("Logged " (if (r-state logged-in)
                                      "in"
                                      "out")))
           (jsx "button" (on-click (@ this change-login))
               ("Log " (if (r-state logged-in)
                           "out"
                           "in")))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Fetching Data from an API
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (ps-bind update-character)
                                     (setf (r-state) (create
                                                      is-loading f
                                                      character (create))))))
    (new-method update-character (data)
      ((@ this set-state) (lambda ()
                            (create character data
                                    is-loading f))))

    (new-method component-did-mount ()
      ((@ this set-state) (lambda () (create is-loading t)))
      (var update-func (@ this update-character))
      (chain (fetch "https://swapi.co/api/people/1")
             (then (lambda (response)
                     ((@ response json))))
             (then (lambda (data)
                     (update-func data)))))

    (new-method render ()
      (jsx "div" () ((if (r-state is-loading)
                         "Loading..."
                         (r-state character name))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Forms Part 1
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (ps-bind handle-change)
                                     (setf (r-state) (create
                                                      first-name ""
                                                      last-name "")))))
    (new-method handle-change (event)
      (var name (@ event target name))   ; copy the event values quickly
      (var value (@ event target value)) ; to prevent certain bugs
      ((@ this set-state) (create [name] value)))

    (new-method render ()
      (jsx "form" ()
          ((jsx "input" (type "text" name "firstName" placeholder "First Name"
                              value (r-state first-name) on-change (@ this handle-change)))
           (jsx "br")
           (jsx "input" (type "text" name "lastName" placeholder "Last Name"
                              value (r-state last-name) on-change (@ this handle-change)))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Forms Part 2
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (ps-bind handle-change)
                                     (setf (r-state) (create is-friendly t
                                                             textarea "Some default value"
                                                             gender ""
                                                             fav-color "blue")))))
    (new-method handle-change (event)
      (var name (@ event target name))
      (var type (@ event target type))
      (var value (@ event target value))
      (var checked (@ event target checked))
      (if (eql type "checkbox")
          ((@ this set-state) (create [name] checked))
          ((@ this set-state) (create [name] value))))

    (new-method handle-submit (event)
      ((@ event prevent-default))
      ((@ console log) "Form submitted"))
    
    (new-method render ()
      (jsx "form" (on-submit (@ this handle-submit))
          ((jsx "textarea" (name "textarea" value (r-state textarea)
                                 on-change (@ this handle-change)))
           (jsx "br")
           (jsx "input" (type "checkbox" checked (r-state is-friendly)
                              name "isFriendly" on-change (@ this handle-change)))
           "Is friendly?"
           (jsx "br")
           (jsx "input" (type "radio" name "gender" value "male"
                              checked (eql (r-state gender) "male")
                              on-change (@ this handle-change)))
           "Male"
           (jsx "br")
           (jsx "input" (type "radio" name "gender" value "female"
                              checked (eql (r-state gender) "female")
                              on-change (@ this handle-change)))
           "Female"
           (jsx "br")
           (jsx "select" (name "favColor" value (r-state fav-color)
                               on-change (@ this handle-change))
               ((jsx "option" (value "blue") ("Blue"))
                (jsx "option" (value "red") ("Red"))
                (jsx "option" (value "green") ("Green"))))
           (jsx "br")
           (jsx "button" () ("Submit!"))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Exercise 8
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (ps-bind handle-change handle-submit)
                                     (setf (r-state) (create first-name ""
                                                             last-name ""
                                                             age ""
                                                             gender ""
                                                             destination ""
                                                             vegan f)))))
    (new-method handle-submit (event)
      ((@ event prevent-default))
      ((@ console log) "Form submitted."))
    
    (new-method handle-change (event)
      (var name (@ event target name))
      (var type (@ event target type))
      (var value (@ event target value))
      (var checked (@ event target checked))
      (if (eql type "checkbox")
          ((@ this set-state) (create [name] checked))
          ((@ this set-state) (create [name] value))))
    
    (new-method render ()
      (jsx "main" ()
          ((jsx "form" (on-submit (@ this handle-submit))
               ((jsx "input" (name "firstName" placeholder "First Name" value (r-state first-name)
                                   on-change (@ this handle-change)))
                (jsx "input" (name "lastName" placeholder "Last Name" value (r-state last-name)
                                   on-change (@ this handle-change)))
                (jsx "input" (name "age" placeholder "Age" value (r-state age)
                                   on-change (@ this handle-change)))
                (jsx "input" (type "radio" name "gender" value "male"
                                   on-change (@ this handle-change)))
                "Male"
                (jsx "input" (type "radio" name "gender" value "female"
                                   on-change (@ this handle-change)))
                "Female"
                (jsx "br")
                (jsx "select" (name "destination" value (r-state destination)
                                    on-change (@ this handle-change))
                    ((jsx "option" () ("Please select a destination."))
                     (jsx "option" (value "New York") ("New York"))
                     (jsx "option" (value "Paris") ("Paris"))
                     (jsx "option" (value "Tokyo") ("Tokyo"))))
                (jsx "br")
                (jsx "input" (type "checkbox" name "vegan" checked (r-state "vegan")
                                   on-change (@ this handle-change)))
                "Vegan"
                (jsx "br")
                (jsx "button" () ("Submit"))))))))
  
  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))




;; Container/Component Architecture
(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *form-container (:extends (@ *react *component)
                              :constructor (() ((super)
                                                (ps-bind handle-change handle-submit)
                                                (setf (r-state) (create first-name ""
                                                                        last-name ""
                                                                        age ""
                                                                        gender ""
                                                                        destination ""
                                                                        vegan f)))))
    (new-method handle-submit (event)
      ((@ event prevent-default))
      ((@ console log) "Form submitted."))
    
    (new-method handle-change (event)
      (var name (@ event target name))
      (var type (@ event target type))
      (var value (@ event target value))
      (var checked (@ event target checked))
      (if (eql type "checkbox")
          ((@ this set-state) (create [name] checked))
          ((@ this set-state) (create [name] value))))

    (new-method render ()
      (jsx *form-component (data (r-state)
                                 handle-change (@ this handle-change)
                                 handle-submit (@ this handle-submit)))))
   
  (ps
    (defun *form-component (props)
      (jsx "main" ()
          ((jsx "form" (on-submit (@ props handle-submit))
               ((jsx "input" (name "firstName" placeholder "First Name" value (r-data first-name)
                                   on-change (@ props handle-change)))
                (jsx "input" (name "lastName" placeholder "Last Name" value (r-data last-name)
                                   on-change (@ props handle-change)))
                (jsx "input" (name "age" placeholder "Age" value (r-data age)
                                   on-change (@ props handle-change)))
                (jsx "input" (type "radio" name "gender" value "male"
                                   on-change (@ props handle-change)))
                "Male"
                (jsx "input" (type "radio" name "gender" value "female"
                                   on-change (@ props handle-change)))
                "Female"
                (jsx "br")
                (jsx "select" (name "destination" value (r-data destination)
                                    on-change (@ props handle-change))
                    ((jsx "option" () ("Please select a destination."))
                     (jsx "option" (value "New York") ("New York"))
                     (jsx "option" (value "Paris") ("Paris"))
                     (jsx "option" (value "Tokyo") ("Tokyo"))))
                (jsx "br")
                (jsx "input" (type "checkbox" name "vegan" checked (r-data "vegan")
                                   on-change (@ props handle-change)))
                "Vegan"
                (jsx "br")
                (jsx "button" () ("Submit")))))))
    
    (defun *app ()
      (jsx *form-container))
    
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))
