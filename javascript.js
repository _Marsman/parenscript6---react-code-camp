var __PS_MV_REG = {}; class MemeGenerator extends React.Component { constructor() { super();
this.handleSubmit = this.handleSubmit.bind(this);
this.handleChange = this.handleChange.bind(this);
this.setAllImages = this.setAllImages.bind(this);
this.state = { topText : '',
            bottomText : '',
            image : 'http://i.imgflip.com/1bij.jpg',
            allMemeImgs : []
          }; }; setAllImages(response) {
    __PS_MV_REG = [];
    return this.setState(function () {
        return { allMemeImgs : response.data.memes };
    });
}; componentDidMount() {
    var updateFunc = this.setAllImages;
    __PS_MV_REG = [];
    return fetch('https://api.imgFlip.com/get_memes').then(function (response) {
        __PS_MV_REG = [];
        return response.json();
    }).then(function (response) {
        __PS_MV_REG = [];
        return updateFunc(response);
    });
}; handleChange(event) {
    var name = event.target.name;
    var value = event.target.value;
    __PS_MV_REG = [];
    return this.setState({ [name] : value });
}; handleSubmit(event) {
    event.preventDefault();
    var rand = Math.floor(Math.random() * this.state.allMemeImgs.length);
    __PS_MV_REG = [];
    return this.setState({ image : this.state.allMemeImgs[rand].url });
}; render() {
    __PS_MV_REG = [];
    return React.createElement('div', null, React.createElement('form', { className : 'meme-form', onSubmit : this.handleSubmit }, React.createElement('input', { name : 'topText',
                                                                                                                                                                  value : this.state.topText,
                                                                                                                                                                  placeholder : 'Top Text',
                                                                                                                                                                  onChange : this.handleChange
                                                                                                                                                                }, null), React.createElement('input', { name : 'bottomText',
                                                                                                                                                                                                         value : this.state.bottomText,
                                                                                                                                                                                                         placeholder : 'Bottom Text',
                                                                                                                                                                                                         onChange : this.handleChange
                                                                                                                                                                                                       }, null), React.createElement('button', null, 'Gen')), React.createElement('div', { className : 'meme' }, React.createElement('img', { src : this.state.image, alt : '' }, null), React.createElement('h2', { className : 'top' }, this.state.topText), React.createElement('h2', { className : 'bottom' }, this.state.bottomText)));
};  }; function Header(props) {
    __PS_MV_REG = [];
    return React.createElement('header', null, React.createElement('img', { src : 'http://www.pngall.com/wp-content/uploads/2016/05/Trollface.png', alt : 'Problem?' }, null), React.createElement('p', null, 'Meme Generator'));
};
function App() {
    __PS_MV_REG = [];
    return React.createElement('div', null, React.createElement(Header, null, null), React.createElement(MemeGenerator, null, null));
};
ReactDOM.render(React.createElement(App, null, null), document.getElementById('app'));