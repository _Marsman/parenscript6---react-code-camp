(in-package :parenscript6)

(code
 #p"javascript.js"
 (stringify
  (setup)
  (new-class *meme-generator (:extends (@ *react *component)
                              :constructor (() ((super)
                                                (ps-bind set-all-images handle-change handle-submit)
                                                (setf (r-state) (create top-text ""
                                                                        bottom-text ""
                                                                        image "http://i.imgflip.com/1bij.jpg"
                                                                        all-meme-imgs '())))))
    (new-method set-all-images (response)
      ((@ this set-state) (lambda ()
                            (create all-meme-imgs (@ response data memes)))))
    
    (new-method component-did-mount ()
      (var update-func (@ this set-all-images))
      (chain (fetch "https://api.imgFlip.com/get_memes")
             (then (lambda (response)
                     ((@ response json))))
             (then (lambda (response)
                     (update-func response)))))

    (new-method handle-change (event)
      (var name (@ event target name))
      (var value (@ event target value))
      ((@ this set-state) (create [name] value)))

    (new-method handle-submit (event)
      ((@ event prevent-default))
      (var rand ((@ *math floor) (* (chain *math (random)) (r-state all-meme-imgs length))))
      ((@ this set-state) (create image (@ (aref (r-state all-meme-imgs) rand) url))))
    
    (new-method render ()
      (jsx "div" ()
          ((jsx "form" (class-name "meme-form" on-submit (@ this handle-submit))
               ((jsx "input" (name "topText" value (r-state top-text) placeholder "Top Text"
                                   on-change (@ this handle-change)))
                (jsx "input" (name "bottomText" value (r-state bottom-text) placeholder "Bottom Text"
                                   on-change (@ this handle-change)))
                (jsx "button" () ("Gen"))))
           (jsx "div" (class-name "meme")
               ((jsx "img" (src (r-state image) alt ""))
                (jsx "h2" (class-name "top") ((r-state top-text)))
                (jsx "h2" (class-name "bottom") ((r-state bottom-text)))))))))
  
  (ps
    (defun *header (props)
      (jsx "header" ()
          ((jsx "img" (src "http://www.pngall.com/wp-content/uploads/2016/05/Trollface.png" alt "Problem?"))
           (jsx "p" () ("Meme Generator")))))

    (defun *app ()
      (jsx "div" ()
          ((jsx *header)
           (jsx *meme-generator))))
    
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))

(code
 #p"style.css"
 (stringify
  "* {
    box-sizing: border-box;
}

body {
    margin: 0;
    background-color: whitesmoke;
}

header {
height: 100px;
    display: flex;
    align-items: center;
    background: #6441A5;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #2a0845, #6441A5);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #2a0845, #6441A5); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */}

header > img {
    height: 80%;
    margin-left: 10%;
}

header > p {
    font-family: VT323, monospace;
    color: whitesmoke;
    font-size: 50px;
    margin-left: 60px;
}

.meme {
    position: relative;
    width: 90%;
    margin: auto;
}

.meme > img {
    width: 100%;
}

.meme > h2 {
    position: absolute;
    width: 80%;
    text-align: center;
    left: 50%;
    transform: translateX(-50%);
    margin: 15px 0;
    padding: 0 5px;
    font-family: impact, sans-serif;
    font-size: 2em;
    text-transform: uppercase;
    color: white;
    letter-spacing: 1px;
    text-shadow:
       2px 2px 0 #000,
       -2px -2px 0 #000,
       2px -2px 0 #000,
       -2px 2px 0 #000,
       0 2px 0 #000,
       2px 0 0 #000,
       0 -2px 0 #000,
       -2px 0 0 #000,
       2px 2px 5px #000;
}

.meme > .bottom {
    bottom: 0;
}

.meme > .top {
    top: 0;
}

.meme-form {
    width: 90%;
    margin: 20px auto;
    display: flex;
    justify-content: space-between;
}

.meme-form > input {
    width: 45%;
    height: 40px;
}

.meme-form > button {
    border: none;
    font-family: VT323, monospace;
    font-size: 25px;
    letter-spacing: 1.5px;
    color: white;
    background: #6441A5;
}

.meme-form > input::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  font-family: VT323, monospace;
  font-size: 25px;
  text-align: cen
}
.meme-form > input::-moz-placeholder { /* Firefox 19+ */
  font-family: VT323, monospace;
  font-size: 25px;
  text-align: cen
}

.meme-form > input:-ms-input-placeholder { /* IE 10+ */
  font-family: VT323, monospace;
  font-size: 25px;
  text-align: cen
}

.meme-form > input:-moz-placeholder { /* Firefox 18- */
  font-family: VT323, monospace;
  font-size: 25px;
  text-align: cen
}"))
