;;;; react-code-camp.asd

(asdf:defsystem #:react-code-camp
  :description "Parenscript code examples from the React tutorial on freecodecamp."
  :author "Marsman"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "react-code-camp")
               (:file "code")
               (:file "todo")
               (:file "meme-generator"))
  :depends-on (:cl-who :hunchentoot
               :parenscript :parenscript6))
