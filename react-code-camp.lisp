;;;; react-code-camp.lisp

(in-package #:react-code-camp)

(hunchentoot:define-easy-handler (index-page :uri "/index") ()
  (with-html-output-to-string (s)
    (:html
     (:head
      (:script :crossorigin t :src "https://unpkg.com/react@16/umd/react.development.js")
      (:script :crossorigin t :src "https://unpkg.com/react-dom@16/umd/react-dom.development.js")
      (:link :rel "stylesheet" :type "text/css" :href #p"style.css"))
     (:body
      (:div :id "app")
      (:script :src #p"javascript.js")))))



(hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 6789)) ; :document-root #p""

(push (hunchentoot:create-static-file-dispatcher-and-handler
       "/javascript.js" "~/Downloads/portacle/all/quicklisp/local-projects/react-code-camp/javascript.js")
      hunchentoot:*dispatch-table*)

(push (hunchentoot:create-static-file-dispatcher-and-handler
       "/style.css" "~/Downloads/portacle/all/quicklisp/local-projects/react-code-camp/style.css")
      hunchentoot:*dispatch-table*)
