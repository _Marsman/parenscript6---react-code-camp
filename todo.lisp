(in-package :parenscript6)

(code
 #p"javascript.js"
 (stringify
  (ps
    (var todos-data (list (create
                           id 1
                           text "Take out the trash"
                           completed t)
                          (create
                           id 2
                           text "Grocery shopping"
                           completed f)
                          (create
                           id 3
                           text "Clean gecko tank"
                           completed f)
                          (create
                           id 4
                           text "Mow lawn"
                           completed t))))
  (setup)
  (new-class *todo-item (:extends (@ *react *component)
                         :constructor (() ((super)
                                           (ps-bind handle-change))))
    (new-method handle-change ()
      ((r-props handle-change) (r-props)))
    
    (new-method render ()
      (var style (if (r-props item completed)
                     (create text-decoration "line-through"
                             font-style "italic"
                             color "#cdcdcd")))
      (jsx "div" (class-name "todo-item")
          ((jsx "input" (type "checkbox" checked (r-props item completed)
                              on-change (@ this handle-change)))
           (jsx "p" (style style) ((r-props item text)))))))

  (new-class *app (:extends (@ *react *component)
                   :constructor (() ((super)
                                     (ps-bind handle-change create-todo-item)
                                     (setf (r-state) (create
                                                      todos todos-data)))))
    (new-method handle-change (props)
      (var id (@ props item id))
      ((@ this set-state) (lambda (prev-state)
                            ((@ prev-state todos map) (lambda (item)
                                                        (if (= id (@ item id))
                                                            (setf (@ item completed) (not (@ item completed)))))))))

    (new-method create-todo-item (item)
      (jsx *todo-item (key (@ item id)
                           item item
                           handle-change (@ this handle-change))))
    
    (new-method render ()
      (var todo-components (chain (r-state todos) (map (@ this create-todo-item))))
      (jsx "div" (class-name "todo-list")
          (todo-components))))

  (ps
    (chain *react-d-o-m (render
                         (jsx *app)
                         (chain document (get-element-by-id "app")))))))

(code
 #p"style.css"
 (stringify
  "body {
background-color: whitesmoke;
}

.todo-list {
background-color: white;
margin: auto;
width: 50%;
display: flex;
flex-direction: column;
align-items: center;
border: 1px solid #efefef;
box-shadow:
/* the top layer shadow */
0 1px 1px rgba(0,0,0,0.15),
/* the second layer */
0 10px 0 -5px #eee,
/* the second layer shadow */
0 10px 1px -4px rgba(0,0,0,0.15),
/* the third layer */
0 20px 0 -10px #eee,
/* the third layer shadow */
0 20px 1px -9px rgba(0,0,0,0.15);
padding: 30px;
}

.todo-item {
display: flex;
justify-content: flex-start;
align-items: center;
padding: 30px 20px 0;
width: 70%;
border-bottom: 1px solid #cecece;
font-family: Roboto, sans-serif;
font-weight: 100;
font-size: 15px;
color: #333333;
}

input[type=checkbox] {
margin-right: 10px;
font-size: 30px;
}

input[type=checkbox]:focus {
outline: 0;
}"))
